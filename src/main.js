// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store' //全局中引入vuex
import router from './router'
import $ from 'jquery'
import axios from 'axios'
import promise from 'es6-promise'
import '@/assets/js/common.js' // 公共js
import '@/assets/css/reset.css' // 公共css
import '@/assets/css/common.css' // 公共css
import '@/assets/css/icon/iconfont.css' // 公共css
import '@/assets/css/pubstyle.css'

import Vant from 'vant';  // 组件
import 'vant/lib/vant-css/index.css';
import { 
  Circle, // 进度条加载组件
  Swipe, // 轮播图组件
  SwipeItem, // 轮播图组件
  Tabbar, 
  TabbarItem,  // 标签页插件
  Tab, 
  Tabs,// 横向导航栏
  RadioGroup,
   Radio,// 单选按钮
  DatetimePicker,// 时间选择
  Picker, // 时间选择
  Dialog, // 弹出框
  Popup, //弹出层
  Toast // 轻提示
  } 
  from 'vant';   // 基于vant的插件
import {setIsAutoLogin} from "../static/andIos"; // 安卓和ios免登陆调试
import VueTouch from 'vue-touch';
import commonJs from './util/util'
import VueLazyload from 'vue-lazyload'
// import '@/assets/css/skin.css'

Vue.prototype.axios = axios; // ajax 数据交互
promise.polyfill();

Vue.prototype.setIsAutoLogin = setIsAutoLogin; // 安卓

// Vue.use(Vant) Vant插件
Vue.use(Vant);
Vue.use(Circle); // 进度条组件
Vue.use(Swipe).use(SwipeItem);  // 轮播图组件
Vue.use(Tabbar).use(TabbarItem); // 标签页插件
Vue.use(Tab).use(Tabs); // 横向导航栏
Vue.use(RadioGroup); // 单选按钮
Vue.use(Radio);// 单选按钮
Vue.use(DatetimePicker);// 时间选择
Vue.use(Picker);
Vue.use(Dialog); // 弹框
Vue.use(Popup);//弹出层
Vue.use(Toast); // 弹框
Vue.use(VueTouch, {name:'v-touch'})

Vue.config.productionTip = false; // setup

// 公共方法提取
Vue.use(commonJs);

// 图片懒加载
Vue.use(VueLazyload,{
  error: require('@/assets/images/err.png'),
  loading: '',
  preLoad: 1,
  attempt: 1
});

export default {
  mounted() {
    this.$dialog.alert({
      message: '' // 弹出框
    });
  }
}


new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  data () {
    return{
      domainName:"https://junction.dev.havensphere.com", // 测试
      rcuApiServer:"http://192.168.43.200:8008",
      // rcuApiServer:"http://127.0.0.1",
      // junAppName: "hotel_id=0086000020&restaurant_id=r001&app_name=weiretreat&device_type=phone", // 正式传参
      // junAppName: "hotel_id=0086000019&restaurant_id=r001&app_name=RadissonBlu_butlerlite&device_type=phone", // 测试
    //  junAppName: "hotel_id=086test1&restaurant_id=r003&app_name=RadissonBlu_butlerlite&device_type=phone", // 测试
      //domainName:"https://junction.havensphere.com", // 正式
      junAppName: "hotel_id=0086000007&restaurant_id=rnwsh001&app_name=RadissonBlu_butlerlite&device_type=phone", // 正式传参
      junAppQuery: "udid=abcdefg123&app_version=v123&os_name=ios123&os_version=ve123&timestamp=1000&" ,// 传参
    
       // 房控系统的参数
      RCA:          `<?xml version="1.0" encoding="UTF-8"?>
                      <Request>
                          <Auth company="Dalitek" cseq="1" request_time="1970-01-11 00:03:25" token="4aae1fae039b634d7ac56a590301be97"/>
                          <Service business="ThirdControl" function="ChangeChannelLevel"/>
                          <RequestData>
                              <RoomName>9999</RoomName>
                              <Password>123456</Password>
                              <Box>1</Box> 
                              <CardNo>`,
      RCB:                  `</CardNo> 
                              <ChannelNo>`,
      RCC:                  `</ChannelNo> 
                              <ChannelLevel>`,
      RCD:                  `</ChannelLevel> 
                              <FadeTime>200</FadeTime> 
                          </RequestData>
                      </Request>`

    }
  },
  template: '<App/>'
})
