import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'pageLoad', // 加载页面
      component: resolve => require(['@/pages/login/views/pageLoad'], resolve)
    },
    {
      path: '/welcome',
      name: 'welcome', // 欢迎页面
      component: resolve => require(['@/pages/login/views/welcome'], resolve)
    },
    {
      path: '/register',
      name: 'register', // 注册页面
      component: resolve => require(['@/pages/login/views/register'], resolve)
    },
    {
      path: '/signIn',
      name: 'signIn',  // 登录页面
      component: resolve => require(['@/pages/login/views/signIn'], resolve)
    },
    {
      path: '/findPW',
      name: 'findPW', // 找回密码
      component: resolve => require(['@/pages/login/views/findPW'], resolve)
    },
    {
      path: '/index',
      name: 'index', // 主页面
      component: resolve => require(['@/pages/shopping/views/index'], resolve)
    },
    {
      path: '/product',
      name: 'product', // 产品列表（食品类）
      component: resolve => require(['@/pages/shopping/views/product'], resolve)
    },
    {
      path: '/productDetail',
      name: 'productDetail', // 产品列表（食品类详情）
      component: resolve => require(['@/pages/shopping/views/productDetail'], resolve)
    },
    {
      path: '/proList',
      name: 'proList', // 产品列表（餐厅类）
      component: resolve => require(['@/pages/shopping/views/proList'], resolve)
    },
    {
      path: '/proListDetail',
      name: 'proListDetail', // 产品列表（餐厅类详情）
      component: resolve => require(['@/pages/shopping/views/proListDetail'], resolve)
    },
    {
      path: '/cartShopping',
      name: 'cartShopping', // 购物车（空）
      component: resolve => require(['@/pages/cart/views/cartShopping.vue'], resolve)
    },
    {
      path: '/cartEmpty',
      name: 'cartEmpty', // 购物车（空）
      component: resolve => require(['@/pages/cart/views/cartEmpty.vue'], resolve)
    },
    {
      path: '/order',
      name: 'order', //订单
      component: resolve => require(['@/pages/cart/views/order'], resolve)
    },
    {
      path: '/adressCreat',
      name: 'adressCreat', // 新建地址
      component: resolve => require(['@/pages/adress/views/adressCreat'], resolve)
    },
    {
      path: '/adressEdit',
      name: 'adressEdit', // 编辑地址
      component: resolve => require(['@/pages/adress/views/adressEdit'], resolve)
    },
    {
      path: '/adressManage',
      name: 'adressManage', // 地址管理
      component: resolve => require(['@/pages/adress/views/adressManage'], resolve)
    },
    {
      path: '/weatherDetail',
      name: 'weatherDetail', // 天气详情
      component: resolve => require(['@/pages/shopping/views/weatherDetail'], resolve)
    },
    {
      path: '/setting',
      name: 'setting', // 设置
      component: resolve => require(['@/pages/setOthers/views/setting'], resolve)
    },
    {
      path: '/about',
      name: 'about', // 关于
      component: resolve => require(['@/pages/setOthers/views/about'], resolve)
    },
    {
      path: '/language',
      name: 'language', // 语言
      component: resolve => require(['@/pages/setOthers/views/language'], resolve)
    },
    {
      path: '/gender',
      name: 'gender', // 语言
      component: resolve => require(['@/pages/setOthers/views/gender'], resolve)
    },
    {
      path: '/messageList',
      name: 'messageList', // 消息列表
      component: resolve => require(['@/pages/setOthers/views/messageList'], resolve)
    },
    {
      path: '/messageDetail',
      name: 'messageDetail', // 消息详情
      component: resolve => require(['@/pages/setOthers/views/messageDetail'], resolve)
    },
    {
      path: '/orderConfirm',
      name: 'orderConfirm', // 支付成功
      component: resolve => require(['@/pages/cart/views/orderConfirm'], resolve)
    },
    {
      path: '/paySucss',
      name: 'paySucss', // 支付成功
      component: resolve => require(['@/pages/cart/views/paySucss'], resolve)
    },
    {
      path: '/payFailed',
      name: 'payFailed', // 支付失败
      component: resolve => require(['@/pages/cart/views/payFailed'], resolve)
    },
    {
      path: '/orderListQuery',
      name: 'orderListQuery', // 订单查询
      component: resolve => require(['@/pages/setOthers/views/orderListQuery'], resolve)
    },
    {
      path: '/orderListDetail',
      name: 'orderListDetail', // 单个订单详情 
      component: resolve => require(['@/pages/setOthers/views/orderListDetail'], resolve)
    },
    {
      path: '/bangqi',
      name: 'bangqi', // 房控(邦奇房控箱) 
      component: resolve => require(['@/pages/room/views/bangqi'], resolve)
    },
    {
      path: '/home',
      name: 'home', // 房控(主页) 
      component: resolve => require(['@/pages/room/views/home'], resolve)
    },
    {
      path: '/ac2',
      name: 'ac2', // 房控（空调）
      component: resolve => require(['@/pages/room/views/ac2'], resolve)
    },
    {
      path: '/curtain',
      name: 'curtain', // 房控（窗帘）
      component: resolve => require(['@/pages/room/views/curtain'], resolve)
    },
    {
      path: '/tv',
      name: 'tv', // 房控（TV）
      component: resolve => require(['@/pages/room/views/tv'], resolve)
    },
    {
      path: '/audio',
      name: 'audio', // 房控（Audio）
      component: resolve => require(['@/pages/room/views/audio'], resolve)
    },
    {
      path: '/light',
      name: 'light', // 房控（灯控）
      component: resolve => require(['@/pages/room/views/light'], resolve)
    }
  ]
})



//  路由拦截
router.beforeEach((to, from, next) => {
  if (to.matched.some(res => res.meta.requireAuth)) { // 判断是否需要登录权限
    if (true) { // 判断是否登录
      next()
    } else { // 没登录则跳转到登录界面
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    }
  } else {
    next()
  }
})

export default router
