export default{
    install:function(Vue,opt){
        let _this = this;
        // 设置全局函数（判断跳转的类型）
        Vue.prototype.getTypeHref = function (type){
            //changeData是函数名
            let href = "";
            if (
            type == "MERCHANDISE_CATEGORY" ||
            type == "MERCHANDISE"
            ) {
            href = "product"; // 商品类别
            } else if (type == "INFO") {
            href = "proList"; // 信息展示类别
            } else if (type == "WEATHER") {
            href = "weatherDetail"; // 信息展示类别
            } else {
            href = "";
            }
            return href;
        }
        // 背景图片的设置
        Vue.prototype.ImgWidthHeight = function(url,obj) {
            let img = new Image();
            img.src = url;
            let imgW = img.width;
            let imgH = img.height;
            let clientW = document.body.clientWidth; // 屏幕宽度
            let clientH = document.body.clientHeight; // 屏幕高度
            if (imgW / clientW > imgH / clientH) {
            obj.backgroundSize = "auto 100%";// 高度为100%；
            } else {
                obj.backgroundSize = "100% auto"; // 宽度为100%；
            }
        }
        // 判断机型是不是iPhoneX
        Vue.prototype.isIPhoneX = function(idMain,idFooter){
            var u = navigator.userAgent;
            var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
            if (isIOS) {        
                if (screen.height == 812 && screen.width == 375){
                // iphoneX的适配
                    let comX = document.getElementById(idMain);
                    // comX.style.height = "752px";
                    // comX.style.margin = "30px 0 0 0";
                    // let comFooter = document.getElementById(idFooter);
                    // if(comFooter){
                    // comFooter.style.display = "block";
                    // }
                }else{
                    //不是iphoneX
                } 
            }
        }


        Vue.prototype.isIPhoneXbtn = function(idFootBtn){
            var u = navigator.userAgent;
            var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
            if (isIOS) {        
                if (screen.height == 812 && screen.width == 375){
                // iphoneX的适配
                    let comX = document.getElementById(idFootBtn);
                    //comX.style.bottom = "30px";
                }else{
                    //不是iphoneX
                } 
            }
        }

        // 时间获取
        Vue.prototype.getTimeDate = function(times){
            console.log(times);
            let date = new Date(times);  // times 时间戳（通常为13位）
            let  year = date.getFullYear() + '-';
            let  month = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
            let  day = (date.getDate() < 10 ?'0'+ date.getDate() : date.getDate())+' ';
            let  hour = date.getHours() < 10 ?'0'+ date.getHours() : date.getHours()+ ':';
            let  min = date.getMinutes() < 10 ?'0'+ date.getMinutes() : date.getMinutes() + ':';
            let  second = date.getSeconds() < 10 ?'0'+ date.getSeconds() : date.getSeconds();
            let dataTime = year + month + day + hour + min + second;
            return dataTime;  //  eg:  2018-08-02 09:30:12
        }
        // 获取一个月的时间
        Vue.prototype.getMonth = function(){
            let date1 = new Date();  // 获取当前日期
            let arr = [];
            for(let i=0; i<30; i++){
                let date2 = new Date(date1);
                date2.setDate(date1.getDate() + i);
                let dayYear = date2.getFullYear();
                let dayMon = (date2.getMonth() + 1)< 10 ?'0'+(date2.getMonth() + 1):(date2.getMonth() + 1);
                let dayDate = date2.getDate() < 10 ?'0'+ date2.getDate():date2.getDate();
                let dayList = dayYear+ "/" + dayMon + "/"+ dayDate;
                arr.push(dayList);  // 存储日期
            }
            let  hour = date1.getHours();
            let  min = date1.getMinutes() < 10 ?'0'+ date1.getMinutes() : date1.getMinutes();
            if(min >=0 && min < 15){
                min = "45";
            }else if(min >=15 && min < 30){
                min = "00";
                hour = hour +1;
            }else if(min >=30 && min <= 45){
                min = "15";
                hour = hour +1;
            }else{
                min = "30";
                hour = hour +1;
            }
            if(hour < 10){
                hour = "0"+hour; // 补灵操作
            }
            let hourMin = hour+":"+min;
            console.log("hourMin",hourMin);
            let arr2 = [];
            arr2.push(arr);
            arr2.push(hourMin);
           return arr2;
        }
    }
}






